/// <reference path="../../node_modules/@types/react/index.d.ts" />
/// <reference path="../../node_modules/@types/lodash/index.d.ts" />
/// <reference path="../../node_modules/@types/bluebird/index.d.ts" />

import * as Bluebird from "bluebird";
import * as React from "react";
import * as ReactDOM from "react-dom";
import * as I from "immutable";
import * as immstruct from "immstruct";
import * as _ from "lodash";
import * as omniscient from "omniscient";
import Axios from "axios";

import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Drawer from "@material-ui/core/Drawer";
import Grid from "@material-ui/core/Grid";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListSubheader from "@material-ui/core/ListSubheader";
import Divider from "@material-ui/core/Divider";
import FormLabel from "@material-ui/core/FormLabel";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Paper from "@material-ui/core/Paper";
import FilledInput from "@material-ui/core/FilledInput";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import Typography from "@material-ui/core/Typography";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import SettingsIcon from "@material-ui/icons/Settings";
import Switch from "@material-ui/core/Switch";
import { withStyles } from "@material-ui/core/styles";


function log(s) {
	const p = document.createElement("p");
	const t = document.createTextNode(s);
	p.appendChild(t);
	document.getElementById("console_log").prepend(p); 
}


const consoleEl = document.getElementById("console");
const consoleToggleEl = document.getElementById("console_toggle");

function open_log() {
	consoleEl.className="console-open";
}

function close_log() {
	consoleEl.className="console-close";
}

function toggle_log() {
	if (consoleEl.className == "console-open") {
		close_log();
	} else {
		open_log();
	}
}

document.getElementById("console_toggle").addEventListener("click", function(evt) {
	evt.preventDefault();
	toggle_log();	
});


function makeApiCall($config, method, endpoint, params) {
	const host = $config.get("host");
	const key = $config.get("key");
	const secret = $config.get("secret");
	const uri = `${host}api/v1/${endpoint}`;
	const creds = `${key}:${secret}`;
	return Axios({
		method: method,
		url: uri,
		params: params,
		headers: {
			"Content-Type": "application/x-www-form-urlencoded",
			"X-Veracity-Credentials": creds,
		},
	});
}


function errHandler(err) {
	open_log();
	log(err);
}


function doTagUpdate(namespace, pk, val) {
	log("Updating tag...");
	const $$state = immstruct.default("rootData").cursor();
	const $config = $$state.get("config");
	makeApiCall($config, "POST", "tag/set/", {"namespace": namespace, "source": pk, "value": val})
		.then((response) => {
			close_log();
			log("Set tag");
			var data = response.data;
			if (data.success) {
				$$state.update(($oldState) => {
					var oldPk = $oldState.getIn(["item", "pk"]);
					if (oldPk == pk) {
						return $oldState.setIn(["item", "tags", namespace], val);
					}
				});
			} else {
				const msg = data.error || "No error specified";
				throw `Error found: ${msg}`;
			}
		}).catch(errHandler);
}

function loadItem(idx) {
	const $$state = immstruct.default("rootData").cursor();
	const domain = $$state.getIn(["domains", idx]);
	if (domain) {
		$$state.setIn(["interfaceState", "domainIndex"], idx);
		loadDomain(domain.get("pk"));
	}
}

function loadDomain(pk) {
	log("Loading domain...");
	const $$state = immstruct.default("rootData").cursor();
	const $config = $$state.get("config");
	$$state.set("item", null);
	makeApiCall($config, "GET", "domain/info/", {"pk": pk})
		.then((response) => {
			close_log();
			log("Got domain");
			var $data = I.fromJS(response.data.response);
			$$state.update(function($oldState) {
				var $newState = $oldState.set("item", $data);
				$newState = $newState.set("dirty", $data);
				return $newState;
			});
		}).catch(errHandler);
}

function loadPrevItem() {
	const $$state = immstruct.default("rootData").cursor();
	const currIdx = $$state.getIn(["interfaceState", "domainIndex"]);
	if (currIdx != null && currIdx > 0) {
		loadItem(currIdx - 1);	
	} else {
		loadPrevPage();
	}
}

function loadNextItem() {
	const $$state = immstruct.default("rootData").cursor();
	const currIdx = $$state.getIn(["interfaceState", "domainIndex"]);
	if (currIdx != null) {
		const count = $$state.get("domains").count();
		if (currIdx >= (count - 1)) {
			loadNextPage();
		} else {
			loadItem(currIdx + 1);
		}
	}
}

function loadPrevPage() {
	const $$state = immstruct.default("rootData").cursor();
	const page = $$state.getIn(["interfaceState", "page"]);
	if (page > 1) {
		$$state.setIn(["interfaceState", "page"], page - 1);
		loadDomains(true);
	}
}

function loadNextPage() {
	const $$state = immstruct.default("rootData").cursor();
	const page = $$state.getIn(["interfaceState", "page"]);
	const hasNextPage = $$state.getIn(["interfaceState", "hasNextPage"]);
	if (hasNextPage) {
		$$state.setIn(["interfaceState", "page"], page + 1);
		loadDomains(false);
	}
}

function loadDomains(top) {
	log("Loading domains...")
	const $$state = immstruct.default("rootData").cursor();
	const $config = $$state.get("config");
	const $interfaceState = $$state.get("interfaceState");
	const tag_namespace = $config.get("tag_namespace");
	const tag_value = $config.get("tag_value");
	const tag_limit = $config.get("tag_limit");
	const page = $interfaceState.get("page");
	makeApiCall($config, "GET", "tag/tagged/", {"namespace": tag_namespace, "value": tag_value, "limit": tag_limit, "page": page})
		.then((response) => {
			close_log();
			log("Got domains");
			const $domains = I.fromJS(response.data.response);
			const count = $domains.count();
			const hasNextPage = count >= tag_limit;
			let currIdx = null;
			if (count > 0) {
				currIdx = top ? (count - 1) : 0;
			}
			$$state.update(($oldState) => {
				var $newState = $oldState.set("domains", $domains);
				$newState = $newState.set("item", null);
				$newState = $newState.set("dirty", null);
				$newState = $newState.setIn(["interfaceState", "ready"], true);
				$newState = $newState.setIn(["interfaceState", "page"], page);
				$newState = $newState.setIn(["interfaceState", "domainIndex"], null);
				$newState = $newState.setIn(["interfaceState", "hasNextPage"], hasNextPage);
				return $newState;
			});
			if (currIdx != null) {
				window.setTimeout(() => {
					loadItem(currIdx);
				}, 0);
			}
		})
		.catch(errHandler);
}


function loadTags() {
	log("Loading tags...")
	const $$state = immstruct.default("rootData").cursor();
	const $$interfaceState = $$state.cursor("interfaceState");
	const $config = $$state.cursor("config").deref();
	const tag_namespace = $config.get("tag_namespace");
	var get_namespace_tag = makeApiCall($config, "GET", "tag/", {"namespace": tag_namespace})
		.then((response) => {
			log(`Got namespace tag: ${tag_namespace}`);
			return response.data.response;
		})
		.catch((err) => {
			throw `No such namespace ${tag_namespace}`;
		});
	var get_own_tags = makeApiCall($config, "GET", "tags/", {})
		.then((response) => {
			log("Got own tags");
			return response.data.response;
		});
	Bluebird.Promise.join(get_namespace_tag, get_own_tags, function(namespace_tag, own_tags) {
		const $tags = I.fromJS(own_tags);
		$$interfaceState.set("enabledTags", I.Set($tags.map(($v) => $v.get("namespace"))));
		$$state.set("tags", $tags);
		window.setTimeout(function() {
			loadDomains(false);
		}, 0);
	})
		.catch(errHandler);
}


const component = omniscient.withDefaults({
	isImmutable: function(m) {
		return I.isImmutable(m);
	},
	isEqualImmutable: function(a, b) {
		return a.equals(b);
	},
});


type InputType = "input" | "textarea" | "select";
type BoundFieldArguments = {
	$$field: any
	inputType: InputType
	args: any
}
export var BoundField = component("BoundField", {
}, ({$$field, inputType, args}) => {
	if (!$$field.deref()) {
		console.log("ISSUE WITH BOUNDFIELD");
		console.log($$field._keyPath);
	}
	var onChange = function(evt) {
		$$field.set("value", evt.target.value);
	};
	var children:JSX.Element[] = null;
	if (inputType == "select") {
		// create select options from args["choices"]
		var choices = args["choices"];
		var placeholder = args["placeholder"];
		delete args["choices"];
		delete args["placeholder"];
		if (!choices) {
			console.log("Select created but no choices");
		}
		children = _.map(choices, (i) => {
			var val = i[0];
			var label = i[1];
			return <option value={val} key={`option_${val}`}>{label}</option>;
		});
		if (placeholder) {
			children.unshift(<option key="_option_default">{placeholder}</option>);
		}
	} else if (inputType == "date") {
		// translate "date" into <input type="date" />
		args["type"] = "date";
		inputType = "input";
	}
	return React.createElement(inputType, _.defaults({
		"onChange": onChange,
		"value": $$field.get("value"),
	}, args), children);
});


const TagConfigEdit = component("TagConfigEdit", {
}, ({$$interfaceState, $tags}) => {
	const $$enabledTags = $$interfaceState.cursor("enabledTags");
	if (_.isNull($tags)) {
		return <div>No tags! This shouldn't have happened</div>;
	}
	const onClose = function(evt) {
		$$interfaceState.set("tagDrawerOpen", false);
	}
	return (
		<div style={{width: "400px"}}>
			<FormControl component="div" style={{padding: "15px"}}>
				<FormLabel>Enable tags</FormLabel>
				<FormGroup>
					{$tags.map(function($tag, i) {
						const itemKey = `configtagtoggle_${i}`;
						const namespace = $tag.get("namespace");
						const checked = $$enabledTags.has(namespace);
						var onChange = function(evt) {
							$$interfaceState.update("enabledTags", function($oldTags) {
								if (evt.target.checked) {
									return $oldTags.add(namespace);
								} else {
									return $oldTags.delete(namespace);
								}
							});
						};
						const switchEl = <Switch checked={checked} onChange={onChange} />;
						return <FormControlLabel key={itemKey} control={switchEl} label={namespace} />
					}).toArray()}
				</FormGroup>
			</FormControl>
			<div style={{padding: "15px"}}>
				<Button onClick={onClose} variant="contained" color="primary">Close</Button>
			</div>
		</div>
	);
});

const ConfigEdit = component("ConfigEdit", {
}, ({$$config, $$interfaceState}) => {
	const values = $$config.deref().valueSeq().toArray();
	const hasValues = _.every(values);
	const onChangeKey = function(evt) {
		$$config.set("key", evt.target.value);
	}
	const onChangeSecret = function(evt) {
		$$config.set("secret", evt.target.value);
	}
	const onChangeHost = function(evt) {
		$$config.set("host", evt.target.value);
	}
	const onChangeTagNamespace = function(evt) {
		$$config.set("tag_namespace", evt.target.value);
	}
	const onChangeTagValue = function(evt) {
		$$config.set("tag_value", evt.target.value);
	}
	const onClose = function(evt) {
		$$interfaceState.set("drawerOpen", false);
		window.setTimeout(loadTags, 0);
	}
	return (
		<div style={{width: "400px"}} >
			<Divider />
			<List dense={true} subheader={<ListSubheader component="div">Key Secrets</ListSubheader>}>
				<ListItem>
					<TextField fullWidth={true} label="Key" value={$$config.get("key")} onChange={onChangeKey} variant="outlined" />
				</ListItem>
				<ListItem>
					<TextField fullWidth={true} label="Secret" value={$$config.get("secret")} onChange={onChangeSecret} variant="outlined" />
				</ListItem>
				<ListItem>
					<TextField fullWidth={true} label="Host" value={$$config.get("host")} onChange={onChangeHost} variant="outlined" />
				</ListItem>
			</List>
			<Divider />
			<List dense={true} subheader={<ListSubheader component="div">Edit Mode</ListSubheader>}>
				<ListItem>
					<TextField fullWidth={true} label="Tag Namespace" value={$$config.get("tag_namespace")} onChange={onChangeTagNamespace} variant="outlined" />
				</ListItem>
				<ListItem>
					<TextField fullWidth={true} label="Tag Value" value={$$config.get("tag_value")} onChange={onChangeTagValue} variant="outlined" />
				</ListItem>
			</List>
			{ hasValues ? (
				<div>
					<Divider />
					<List dense={true} subheader={<ListSubheader component="div">Ready!</ListSubheader>}>
						<ListItem>
							<Button onClick={onClose} variant="contained" color="primary">GO!</Button>
						</ListItem>
					</List>
				</div>
			) : null}
		</div>
	);
});

const LoadingView = component("LoadingView", {
}, () => {
	return (
		<h1>Loading...</h1>
	);
});

const DomainList = component("DomainList", {
}, ({$$interfaceState, $domains}) => {
	function makeOnSelectItem(i) {
		return function(evt) {
			window.setTimeout(function() {
				loadItem(i);
			}, 0);
		};
	}
	return (
		<List subheader={<ListSubheader component="div">Domains (page {$$interfaceState.get("page")})</ListSubheader>}>
			{$domains.map(function($domain, i) {
				return (
					<ListItem button key={`domain_${i}`} onClick={makeOnSelectItem(i)}>
						{$domain.get("pk")}: {$domain.get("name")}
					</ListItem>
				)
			}).toArray()}
			{($domains.count() == 0) ? <ListItem button key={`domain_empty`}>End of items!</ListItem> : null}
		</List>
	);
});

const DomainItemEntry = component("DomainItemEntry", {
}, ({itemPk, dataType, namespace, $$field, val}) => {
	const htmlName = `field_input_${namespace}`;
	const dirty_val = $$field.deref() || "";
	const clean_val = val || "";
	const isDirty = !(dirty_val == clean_val);
	const onChangeKey = function(evt) {
		$$field.set(evt.target.value);
	}
	const onDoUpdate = function(evt) {
		window.setTimeout(function() {
			doTagUpdate(namespace, itemPk, dirty_val);
		}, 0);
	}
	const onDoReset = function(evt) {
		$$field.set(val);
	}
	const actionButtons = isDirty ? (
		<Grid item xs={3}>
			<Button variant="contained" color="primary" onClick={onDoUpdate} style={{marginRight: 10}}>Update</Button>
			<Button variant="contained" color="default" onClick={onDoReset}>Reset</Button>
		</Grid>
	) : null;
	let elem = null;
	switch (dataType) {
		case "Boolean":
			elem = (
				<FormControl variant="filled" fullWidth={true}>
					<InputLabel htmlFor={htmlName}>{namespace}</InputLabel>
					<Select value={dirty_val} onChange={onChangeKey} input={<FilledInput name={htmlName} id={htmlName} fullWidth={true} />}>
						<MenuItem value=""><em>None</em></MenuItem>
						<MenuItem value="true">True</MenuItem>
						<MenuItem value="false">False</MenuItem>
					</Select>
				</FormControl>
			);
			break;
		case "Array":
			console.log(`Array field ${namespace} not supported yet`);
			return null;
			break;
		case "String":
			elem = <TextField label={namespace} value={dirty_val} onChange={onChangeKey} variant="outlined" fullWidth={true} />;
			break;
		case "Integer":
			elem = <TextField type="number" label={namespace} value={dirty_val} onChange={onChangeKey} variant="outlined" fullWidth={true} />;
			break;
	}
	if (!elem) {
		open_log();
		log(`Unknown dataType: ${dataType}`);
		return null;
	}
	return (
		<ListItem>
			<Grid container spacing={8}>
				<Grid item xs={9}>{ elem }</Grid>
				{actionButtons}
			</Grid>	
		</ListItem>
	);
});

const DomainEntry = component("DomainEntry", {
}, ({$$interfaceState, $$dirty, $item, $tags}) => {
	const $enabledTags = $$interfaceState.get("enabledTags").deref();
	return (
		<Card>
			<CardContent>
				<Typography component="h2" variant="h5">{ $item.get("root_url") }</Typography>
				<List subheader={<ListSubheader component="div">Tags</ListSubheader>}>
				{$tags.map(function($tag, i) {
					const itemKey = `tagedit_${i}`;
					const namespace = $tag.get("namespace");
					if (!$enabledTags.has(namespace)) {
						return null;
					}
					const $$field = $$dirty.cursor(["tags", namespace]);
					const val = $item.getIn(["tags", namespace]);
					const dataType = $tag.get("datatype");
					return <DomainItemEntry itemPk={$item.get("pk")} dataType={dataType} key={itemKey} namespace={namespace} $$field={$$field} val={val} />;
				}).toArray()}
				</List>
			</CardContent>
		</Card>
	);
});

const EditMode = component("EditMode", {
}, ({$$interfaceState, $domains, $tags, $item, $$dirty}) => {
	const handleNavigation = function(evt, value) {
		switch(value) {
			case "config":
				$$interfaceState.set("drawerOpen", true);
				break;
			case "tags":
				$$interfaceState.set("tagDrawerOpen", true);
				break;
			case "prev":
				window.setTimeout(loadPrevItem, 0);
				break;
			case "next":
				window.setTimeout(loadNextItem, 0);
				break;
		}
	};
	return (
		<div>
			<Grid container spacing={8}>
				<Grid item xs={4}>
					<Paper>
						<DomainList $$interfaceState={$$interfaceState} $domains={$domains} />
					</Paper>
				</Grid>
				<Grid item xs={8}>
					{ $item ? <DomainEntry $$interfaceState={$$interfaceState} $$dirty={$$dirty} $item={$item} $tags={$tags} /> : null }
				</Grid>
			</Grid>
			<BottomNavigation value={null} onChange={handleNavigation} showLabels style={{width: "100%", position: "fixed", bottom: 0}}>
				<BottomNavigationAction value="prev" label="Prev" icon={<ArrowBackIcon />} />
				<BottomNavigationAction value="config" label="Config" icon={<SettingsIcon />} />
				<BottomNavigationAction value="tags" label="Tags" icon={<SettingsIcon />} />
				<BottomNavigationAction value="next" label="Next" icon={<ArrowForwardIcon />} />
			</BottomNavigation>
		</div>
	);
});

const KashinkApp = component("KashinkApp", {
}, ({$$state}) => {
	const $$interfaceState = $$state.get("interfaceState");
	const $$config = $$state.cursor("config");
	const $$dirty = $$state.cursor("dirty");
	const $tags = $$state.cursor("tags").deref();
	const $domains = $$state.cursor("domains").deref();
	const $item = $$state.cursor("item").deref();
	return (
		<div>
			<Drawer open={$$interfaceState.get("drawerOpen")}>
				<ConfigEdit $$config={$$config} $$interfaceState={$$interfaceState} />
			</Drawer>
			<Drawer open={$$interfaceState.get("tagDrawerOpen")} anchor="right">
				<TagConfigEdit $$interfaceState={$$interfaceState} $tags={$tags} />
			</Drawer>
			{ $$interfaceState.get("ready") ? <EditMode $$interfaceState={$$interfaceState} $domains={$domains} $tags={$tags} $item={$item} $$dirty={$$dirty} /> : <LoadingView /> }
		</div>
	);
});


function init(parent_id, initial_state) {
	const mountNode = document.getElementById(parent_id);
	const $defaultState = I.fromJS(initial_state);
	const structure = immstruct.default("rootData", $defaultState);
	const render = function(_) {
		ReactDOM.render(<KashinkApp $$state={structure.cursor()} />, mountNode);
	};
	render(null);
	structure.on("swap", render);
};

function loadHash(h, state) {
	console.log("Loading hash", h);
	const hash = decodeURIComponent(h).substr(1);
	const pieces = hash.split(" ");
	if (pieces.length == 5) {
		state.config.key = pieces[0];
		state.config.secret = pieces[1];
		state.config.host = pieces[2];
		state.config.tag_namespace = pieces[3];
		state.config.tag_value = pieces[4];
	}
	console.log("pieces", pieces);
}
let initialState = {
	config: {
		key: "",
		secret: "",
		host: "",
		tag_namespace: "",
		tag_value: "",
		tag_limit: 20,
	},
	interfaceState: {
		drawerOpen: true,
		tagDrawerOpen: false,
		ready: false,
		hasNextPage: false,
		page: 1,
		domainIndex: null,
		enabledTags: null,
	},
	domains: [],
	tags: null,
	item: null,
	dirty: null,
};
const hash = window.location.hash;
if (hash) {
	loadHash(hash, initialState);
}
init("overlord", initialState);
